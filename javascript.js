
function getNow() {
  const now = new Date();
  const monthDictionary = {
    0: 'January',
    1: 'February',
    2: 'March',
    3: 'April',
    4: 'May',
    5: 'June',
    6: 'July',
    7: 'August',
    8: 'September',
    9: 'October',
    10: 'November',
    11: 'December'
  }
  return {
    mm: now.getMinutes(),
    hh: now.getHours(),
    d: now.getDate(),
    M: monthDictionary[now.getMonth()],
    Y: now.getFullYear()
  }
}

function removeAlarm(e) {
  event.target.parentNode.parentNode.removeChild(event.target.parentNode);
}

window.onload = function() {
  console.log('page loaded')
  const notificationBtn = document.getElementById('enable');
  notificationBtn.addEventListener('click', askNotificationPermission);

  // From official documentation:
  // https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API/Using_the_Notifications_API
  function askNotificationPermission() {
    function handlePermission(permission) {
      if(!('permission' in Notification)) {
        Notification.permission = permission;
      }

      if(Notification.permission === 'denied' || Notification.permission === 'default') {
        notificationBtn.style.display = 'block';
      } else {
        notificationBtn.style.display = 'none';
      }
    }

    if (!('Notification' in window)) {
      console.log("This browser does not support notifications.");
    } else {
      if(checkNotificationPromise()) {
        Notification.requestPermission()
        .then((permission) => {
          handlePermission(permission);
        })
      } else {
        Notification.requestPermission(function(permission) {
          handlePermission(permission);
        });
      }
    }
  }

  if(Notification.permission === 'denied' || Notification.permission === 'default') {
    notificationBtn.style.display = 'block';
  } else {
    notificationBtn.style.display = 'none';
  }

  const alarmForm = document.getElementById('alarm-form');
  alarmForm.addEventListener('submit', addAlarm, false);
  function addAlarm(e) {
    e.preventDefault();
    console.log('add alarm')
    const hours = document.getElementById('form-hours');
    const minutes = document.getElementById('form-minutes');
    const day = document.getElementById('form-day');
    const month = document.getElementById('form-month');
    const year = document.getElementById('form-year');
    console.log(hours.value)

    const alarmElement = document.createElement('li');

    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = 'X';
    deleteButton.onclick = e => removeAlarm(e);

    const checkboxElement = document.createElement('input');
    checkboxElement.setAttribute('type', 'checkbox');

    const minutesElement = document.createElement('input');
    const hoursElement = document.createElement('input');
    const dayElement = document.createElement('input');
    const monthElement = document.createElement('input');
    const yearElement = document.createElement('input');
    minutesElement.value = minutes.value;
    minutesElement.style.width = "20px";
    hoursElement.value = hours.value;
    hoursElement.style.width = "20px";
    dayElement.value = day.value;
    dayElement.style.width = "20px";
    monthElement.value = month.value;
    monthElement.style.width = "120px";
    yearElement.value = year.value;
    yearElement.style.width = "50px";

    alarmElement.appendChild(checkboxElement);
    alarmElement.appendChild(minutesElement);
    alarmElement.appendChild(hoursElement);
    alarmElement.appendChild(dayElement);
    alarmElement.appendChild(monthElement);
    alarmElement.appendChild(yearElement);
    alarmElement.appendChild(deleteButton);

    const alarmListElement = document.getElementById('alarm-list');
    alarmListElement.appendChild(alarmElement);
  }
}

function showNotification(text) {
  if(Notification.permission === 'granted') {
   const notification = new Notification('To do list', { body: text });
  }
}

setInterval(() => {
  const alarmListElement = document.getElementById('alarm-list');
  for (const li of alarmListElement.children) {
    const minutes = li.childNodes[1].value;
    const hours = li.childNodes[2].value;
    const day = li.childNodes[3].value;
    const month = li.childNodes[4].value;
    const year = li.childNodes[5].value;

    const { mm, hh, d, M, Y } = getNow();
    console.log(mm, hh,d,M,Y);
    if (mm == Number(minutes) && hh == Number(hours) && d == Number(day) && M == month && Y == year) {
      console.log('Trigger notification!')
      const checkbox = li.firstChild;
      if (!checkbox.checked) {
        checkbox.checked = true;
        showNotification(`Alarm: ${mm}:${hh} ${d}th of ${M}, ${Y}`);
      }
    }
  }
}, 5000)

